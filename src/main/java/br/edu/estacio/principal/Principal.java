package br.edu.estacio.principal;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import br.edu.estacio.model.Compra;
import br.edu.estacio.model.Pessoa;

public class Principal {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("Compras");
		EntityManager em = emf.createEntityManager();
		
		//Coloque o seu código aqui!!
		Pessoa maria = new Pessoa();
		maria.setNome("Maria");
		maria.setSobrenome("Joana");
		maria.addCompra(new Compra("Arroz",18.5));
		maria.addCompra(new Compra("Feijao",5.70));
		
		Pessoa joao = new Pessoa();
		joao.setNome("João");
		joao.setSobrenome("Henrique");
		joao.addCompra(new Compra("Batata",2.10));
		joao.addCompra(new Compra("Feijao",6.70));
		joao.addCompra(new Compra("Abacaxi",3.80));
		
		em.getTransaction().begin();
		em.persist(maria);
		em.persist(joao);
		em.getTransaction().commit();
				
		em.close();
		emf.close();
	}

}
